# Help Desk Admin Labkom

Ini adalah platform yang digunakan untuk melaporkan kendala teknis yang dialami terhadap layanan di labkom (terutama di masa pandemik). Semua issue akan direspon oleh laboran/admin dan akan dibantu sesuai kemampuan kami. Platform ini dikhususkan untuk para staf dan dosen di lingkungan FTIS, oleh karena itu diharapkan menggunakan akun dengan email unpar agar jelas penanganannya. Jika dosen yang belum memiliki akun dapat menggunakan fitur sign in with google dan memasukkan email unpar.

Cara melaporkan kendala pada platform ini adalah dengan membuat issue baru pada bagian [Issue](https://gitlab.com/ftis-admin/help-desk/-/issues). Lalu pilih menu [New Issue](https://gitlab.com/ftis-admin/help-desk/-/issues/new) pada bagian kanan atas dan isi dengan kendala teknis yang dihadapi. Diusahakan keterangan ditulis selengkap mungkin agar lebih cepat penanganannya. Issue yang sudah selesai akan dipindah statusnya menjadi "Closed".

Keterangan tersebut diharapkan dapat mencakup :
- Gambaran umum
- Kronologis kejadian dan disertai langkahnya
- Kondisi normalnya seperti apa
- Paling lambat batas waktu pengerjaan
- Screenshot (jika ada)
- Data pendukung lainnya (jika ada)

Jika terdapat pertanyaan dapat ditanyakan ke raymond.chandra@unpar.ac.id

Terimakasih.
